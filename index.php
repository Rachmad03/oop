<?php 

    require('animal.php');
    require('frog.php');
    require('Ape.php');

    $sheep = new Animal('shaun');

    echo "Nama Hewan               : $sheep->name <br>";
    echo "Jumlah Kaki              : $sheep->legs <br>";
    echo "Hewan Berdarah Dingin   : $sheep->cold_blooded";
    echo "<br><br>";


    $kodok = new frog('buduk');

    echo "Nama Hewan : $kodok->name <br>";
    echo "Jumlah Kaki : $kodok->legs <br>";
    echo "Hewan Berdarah Dingin   : $kodok->cold_blooded  <br>";
    echo " , `Kodok buduk sedang loncat ditengah sawah!`". $kodok->jump();
    echo "<br><br>";

    $sungokong = new primata('kera sakti');

    echo "Nama Hewan : $sungokong->name <br>";
    echo "Jumlah Kaki : $sungokong->legs <br>";
    echo "Hewan Berdarah Dingin   : $sungokong->cold_blooded  <br>";
    echo " , `Si pemilik pohon sedang beraksi`".$sungokong->yell();

